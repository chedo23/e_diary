﻿using System.Threading.Tasks;
using Journal.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace Journal.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IAuthService _service;

        public AccountController(IAuthService service)
        {
            _service = service;
        }

        [HttpGet("token")]
        public async Task<IActionResult> GetToken([FromQuery] string login, string password)
        {
            var result = await this._service.GetToken(login, password);

            if (result == null)
            {
                return this.BadRequest(new {errorText = "Invalid username or password."});
            }

            return this.Ok(this.Json(result));
        }
    }
}

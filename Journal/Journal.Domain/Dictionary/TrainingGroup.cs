﻿using System.Collections.Generic;
using Journal.Domain.Common;
using Journal.Domain.Registration;

namespace Journal.Domain.Dictionary
{
    /// <summary>
    ///  Например: 255, 246
    /// </summary>
    public class TrainingGroup : DictionaryBase, IEntity
    {
        public int Id { get; set; }

        public List<Student> Students { get; set; }

        public List<AttendanceRegistration> AttendanceRegistrations { get; set; }
    }
}

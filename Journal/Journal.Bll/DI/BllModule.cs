﻿using Autofac;
using Journal.DAL.DI;

namespace Journal.Bll.DI
{
    public class BllModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(this.GetType().Assembly).AsImplementedInterfaces();
            builder.RegisterModule<DalModule>();
        }
    }
}

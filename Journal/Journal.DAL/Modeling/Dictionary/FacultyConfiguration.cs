﻿using Journal.DAL.Modeling.Common;
using Journal.Domain.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Dictionary 
{
    public class FacultyConfiguration : BaseDictionaryConfiguration<Faculty>, IEntityTypeConfiguration<Faculty>
    {
        public void Configure(EntityTypeBuilder<Faculty> builder)
        {
            base.BaseConfigure(builder);
        }
    }
}

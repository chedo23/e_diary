﻿using Journal.Domain.Common;

namespace Journal.Domain.Dto.Dictionary.EducationalInstitution
{
    public class EducationalInstitutionDto : DictionaryBase, IDto
    {
        public int Id { get; set; }

        public int FacultyId { get; set; }

        public Domain.Dictionary.Faculty Faculty { get; set; }
    }
}

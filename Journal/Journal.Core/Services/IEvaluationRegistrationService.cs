﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Registration.EvaluationRegistration;

namespace Journal.Core.Services
{
    public interface IEvaluationRegistrationService : IServiceBase<EvaluationRegistrationDto>
    {
    }
}

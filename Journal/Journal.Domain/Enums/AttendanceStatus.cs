﻿using System.ComponentModel;

namespace Journal.Domain.Enums
{
    /// <summary>
    /// Заметки об отсутсвии, присутствии, болезни
    /// </summary>
    public enum AttendanceStatus
    {
        [Description("Отсутствует")]
        NotAvailable = 0,
        [Description("Присутствует")]
        Available = 1,
        [Description("Болен")]
        Sick = 2
    }
}

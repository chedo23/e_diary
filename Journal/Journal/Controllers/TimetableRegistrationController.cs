﻿using Journal.Core.Services;
using Journal.Domain.Dto.Registration.TimetableRegistration;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TimetableRegistrationController : Controller, IOperationController<TimetableRegistrationDto>
    {
        private readonly ITimetableRegistrationService _timetableRegistrationService;

        public TimetableRegistrationController(ITimetableRegistrationService timetableRegistrationService)
        {
            this._timetableRegistrationService = timetableRegistrationService;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._timetableRegistrationService.RemoveAsync(id);

            return this.Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._timetableRegistrationService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._timetableRegistrationService.ListAsync();

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TimetableRegistrationDto dto)
        {
            await this._timetableRegistrationService.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] TimetableRegistrationDto dto)
        {
            await this._timetableRegistrationService.UpdateAsync(dto);

            return this.Ok();
        }
    }
}
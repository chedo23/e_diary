﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.EducationalInstitution;
using Microsoft.AspNetCore.Authorization;


namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EducationalInstitutionController : Controller, IOperationController<EducationalInstitutionDto>
    {
        private readonly IEducationalInstitutionService _educational;

        public EducationalInstitutionController(IEducationalInstitutionService educational)
        {
            this._educational = educational;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._educational.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._educational.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EducationalInstitutionDto dto)
        {
            await this._educational.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] EducationalInstitutionDto dto)
        {
            await this._educational.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._educational.RemoveAsync(id);

            return this.Ok();
        }
    }
}

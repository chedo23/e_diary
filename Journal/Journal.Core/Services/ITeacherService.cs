﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.Teacher;

namespace Journal.Core.Services
{
    public interface ITeacherService : IServiceBase<TeacherDto>
    {
    }
}

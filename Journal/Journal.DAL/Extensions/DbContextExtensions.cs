﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Journal.DAL.Extensions
{
    public static class DbContextExtensions
    {
        public static Task RunTrigger(this DbContext context)
        {
            return Task.Run(() => {});
        }
    }
}

﻿
using System.Collections.Generic;
using Journal.Domain.Common;
using Journal.Domain.Enums;
using Journal.Domain.Registration;

namespace Journal.Domain.Dictionary
{
    /// <summary>
    /// Например: Иванов И.И. Строитель Строительный
    /// </summary>
    public class Student : DictionaryBase, IEntity
    {
        public int Id { get; set; }

        public Course Course { get; set; }

        public StudentStatus Status { get; set; }

        public int SpecialtyId { get; set; }

        public Specialty Specialty { get; set; }

        public int FacultyId { get; set; }

        public Faculty Faculty { get; set; }

        public List<EvaluationRegistration> EvaluationRegistrations { get; set; }

        public List<AttendanceRegistration> AttendanceRegistrations { get; set; }
    }
}

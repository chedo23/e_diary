﻿using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Faculty;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class FacultyControllerTest : IOperationControllerTest
    {
        private readonly Mock<IFacultyService> _service;

        private readonly Mock<FacultyController> _controller;

        public FacultyControllerTest()
        {
            _service = new Mock<IFacultyService>();
            _controller = new Mock<FacultyController>(this._service.Object);
        }

        [TestInitialize]
        [Fact]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [TestInitialize]
        [Fact]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [TestInitialize]
        [Fact]
        public async Task Post()
        {
            var dto = new FacultyDto
            {
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Post(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Put()
        {
            var dto = new FacultyDto
            {
                Id = 1,
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Put(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }
    }
}

﻿using Journal.Domain.Common;

namespace Journal.Domain.Dictionary
{
    /// <summary>
    /// Например: Радиотехник - 155, Криобиолог - 182, Молекулярный техник - 152
    /// </summary>
    public class Specialty : DictionaryBase, IEntity
    {
        public int Id { get; set; }
    }
}

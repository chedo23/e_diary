﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.Specialty;

namespace Journal.Core.Services
{
    public interface ISpecialtyService : IServiceBase<SpecialtyDto>
    {
    }
}

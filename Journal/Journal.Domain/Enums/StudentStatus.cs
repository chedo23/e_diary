﻿using System.ComponentModel;

namespace Journal.Domain.Enums
{
    public enum StudentStatus
    {
        [Description("Учится")]
        Study = 0,
        [Description("Отчислен")]
        Expelled = 1
    }
}

﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dto.Registration.TimetableRegistration;
using Journal.Domain.Registration;

namespace Journal.Bll.Services.Registration
{
    public class TimetableRegistrationService : BaseRegistrationServiceOperations<TimetableRegistration, TimetableRegistrationDto>, ITimetableRegistrationService
    {
        public TimetableRegistrationService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Registration.TimetableRegistration;

namespace Journal.Core.Services
{
    public interface ITimetableRegistrationService : IServiceBase<TimetableRegistrationDto>
    {
    }
}

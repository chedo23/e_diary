﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Journal.Domain.Dto.Common;

namespace Journal.Core.Services.Common
{
    /// <summary>
    /// Базовый интерфейс для всех интерейсов сервисов
    /// </summary>
    /// <typeparam name="TEntityDto"></typeparam>
    public interface IServiceBase<TEntityDto> 
        where TEntityDto : class
    {
        Task AddAsync(TEntityDto dto);

        Task<TEntityDto> GetAsync(int id);

        Task<List<TEntityDto>> ListAsync();

        Task UpdateAsync(TEntityDto dto);

        Task RemoveAsync(int id);
    }
}

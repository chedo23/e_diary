﻿using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Registration.AttendanceRegistration;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class AttendanceRegistrationControllerTest : IOperationControllerTest
    {
        private readonly Mock<IAttendanceRegistrationService> _service;
        private readonly Mock<AttendanceRegistrationController> _controller;

        public AttendanceRegistrationControllerTest()
        {
            this._service = new Mock<IAttendanceRegistrationService>();
            this._controller = new Mock<AttendanceRegistrationController>(this._service.Object);
        }

        [TestInitialize]
        [Fact]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [TestInitialize]
        [Fact]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [TestInitialize]
        [Fact]
        public async Task Post()
        {
            var dto = new AttendanceRegistrationDto
            {
                Created = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                TrainingGroupId = 666,
                StudentId = 666,
                IsActive = true
                /* не тестируются:
                 * Student
                 * TrainingGroup
                 * Attendance
                */        
            };

            await this._controller.Object.Post(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Put()
        {
            var dto = new AttendanceRegistrationDto
            {
                Id = 1,
                Created = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                TrainingGroupId = 666,
                StudentId = 666,
                IsActive = true
                /* не тестируются:
                 * Student
                 * TrainingGroup
                 * Attendance
                */
            };

            await this._controller.Object.Put(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }
    }
}

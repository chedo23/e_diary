﻿namespace Journal.Domain.Dto
{
    /// <summary>
    /// Интерфейс для всех DTO
    /// </summary>
    public interface IDto
    {
        int Id { get; set; }
    }
}

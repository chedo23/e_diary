﻿using System.ComponentModel;

namespace Journal.Domain.Enums
{
    /// <summary>
    /// Оценка
    /// </summary>
    public enum Evaluation
    {
        [Description("Оценка 1")]
        Evaluation1 = 0,
        [Description("Оценка 2")]
        Evaluation2 = 1,
        [Description("Оценка 3")]
        Evaluation3 = 2,
        [Description("Оценка 4")]
        Evaluation4 = 3,
        [Description("Оценка 5")]
        Evaluation5 = 4,
    }
}

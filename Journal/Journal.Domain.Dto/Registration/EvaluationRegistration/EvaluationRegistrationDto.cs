﻿using Journal.Domain.Common;
using Journal.Domain.Dictionary;
using Journal.Domain.Enums;

namespace Journal.Domain.Dto.Registration.EvaluationRegistration
{
    public class EvaluationRegistrationDto : RegistrationBase, IDto
    {
        public int Id { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        public int DisciplineId { get; set; }

        public Discipline Discipline { get; set; }

        public Evaluation Evaluation { get; set; }
    }
}

﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.TrainingGroup;

namespace Journal.Bll.Services.Dictionary
{
    public class TrainingGroupService : BaseServiceDictionaryOperations<TrainingGroup, TrainingGroupDto>, ITrainingGroupService
    {
        public TrainingGroupService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

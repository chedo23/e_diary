﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.Student;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace Journal.Bll.Services.Dictionary
{
    public class StudentService : BaseServiceDictionaryOperations<Student, StudentDto>, IStudentService
    {
        private readonly ISession _session;

        private readonly IMapper _mapper;

        public StudentService(ISession session, IMapper mapper) : base(session, mapper)
        {
            _session = session;
            _mapper = mapper;
        }

        public async Task<List<StudentDto>> Search(StudentSearchRequest request)
        {
            if (request.IdFilter != null)
            {
                this._session.Query<Student>()
                        .Where(x => x.Id == request.IdFilter)
                        .Select(x => this._mapper.Map<StudentDto>(x));
            }

            var entities = _session.Query<Student>();

            var query = PredicateBuilder.New<Student>()
                .And(x => (int)x.Status == request.StatusFilter)
                .And(x => x.Name.Contains(request.NameFilter))
                .And(x => x.SpecialtyId == request.SpecialtyFilter)
                .And(x => x.FacultyId == request.FacultyFilter)
                .And(x => (int)x.Course == request.CourseFilter);

            return await entities
                .Include(x => x.Faculty)
                .Include(x => x.Specialty)
                .Where(query)
                .Select(x => this._mapper.Map<StudentDto>(x))
                .ToListAsync();
        }
    }
}

﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Discipline;
using Journal.Domain.Enums;
using Microsoft.AspNetCore.Authorization;


namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DisciplineController : Controller, IOperationController<DisciplineDto>
    {
        private readonly IDisciplineService _disciplineService;

        public DisciplineController(IDisciplineService disciplineService)
        {
           this._disciplineService = disciplineService;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._disciplineService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._disciplineService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DisciplineDto dto)
        {
            await this._disciplineService.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] DisciplineDto dto)
        {
            await this._disciplineService.UpdateAsync(dto);

            return this.Ok();
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._disciplineService.RemoveAsync(id);

            return this.Ok();
        }
    }
}

﻿namespace Journal.Domain.Dto.Common
{
    public class SearchRequestBase
    {
        public int? IdFilter { get; set; }
    }
}

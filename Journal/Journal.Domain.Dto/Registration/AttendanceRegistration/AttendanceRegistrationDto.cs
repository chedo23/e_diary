﻿using Journal.Domain.Common;
using Journal.Domain.Dictionary;
using Journal.Domain.Enums;

namespace Journal.Domain.Dto.Registration.AttendanceRegistration
{
    public class AttendanceRegistrationDto : RegistrationBase, IDto
    {
        public int Id { get; set; }

        public int TrainingGroupId { get; set; }

        public TrainingGroup TrainingGroup { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        public AttendanceStatus Attendance { get; set; }
    }
}

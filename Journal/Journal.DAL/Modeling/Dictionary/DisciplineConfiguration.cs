﻿using Journal.DAL.Modeling.Common;
using Journal.Domain.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Dictionary
{
    public class DisciplineConfiguration : BaseDictionaryConfiguration<Discipline>, IEntityTypeConfiguration<Discipline>
    {
        public void Configure(EntityTypeBuilder<Discipline> builder)
        {
            base.BaseConfigure(builder);
        }
    }
}

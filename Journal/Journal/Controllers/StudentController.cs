﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Student;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StudentController : Controller, IOperationController<StudentDto>, ISearchableController<StudentSearchRequest>
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            this._studentService = studentService;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._studentService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._studentService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] StudentDto dto)
        {
            await this._studentService.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]  StudentDto dto)
        {
            await this._studentService.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._studentService.RemoveAsync(id);

            return this.Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Search([FromBody] StudentSearchRequest request)
        {
            var result = await this._studentService.Search(request);

            return this.Ok(result);
        }
    }
}
﻿using Journal.DAL.Modeling.Common;
using Journal.Domain.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Dictionary
{
    public class StudentConfiguration : BaseDictionaryConfiguration<Student>, IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            base.BaseConfigure(builder);
        }
    }
}
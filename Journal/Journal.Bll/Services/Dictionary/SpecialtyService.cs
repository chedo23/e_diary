﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.Specialty;

namespace Journal.Bll.Services.Dictionary
{
    public class SpecialtyService : BaseServiceDictionaryOperations<Specialty, SpecialtyDto>, ISpecialtyService
    {
        public SpecialtyService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

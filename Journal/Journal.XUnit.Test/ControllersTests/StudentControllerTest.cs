﻿using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Student;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class StudentControllerTest : IOperationControllerTest
    {
        private readonly Mock<IStudentService> _service;

        private readonly Mock<StudentController> _controller;

        public StudentControllerTest()
        {
            _service = new Mock<IStudentService>();
            _controller = new Mock<StudentController>(this._service.Object);
        }

        [Fact]
        [TestInitialize]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [Fact]
        [TestInitialize]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [Fact]
        [TestInitialize]
        public async Task Post()
        {
            var dto = new StudentDto
            {
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Post(dto);
        }

        [Fact]
        [TestInitialize]
        public async Task Put()
        {
            var dto = new StudentDto
            {
                Id = 1,
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Put(dto);
        }

        [Fact]
        [TestInitialize]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }
    }
}

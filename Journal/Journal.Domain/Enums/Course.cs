﻿using System.ComponentModel;

namespace Journal.Domain.Enums
{
    /// <summary>
    /// Номер курса
    /// </summary>
    public enum Course
    {
        [Description("1 Курс")]
        Course1 = 0,
        [Description("2 Курс")]
        Course2 = 1,
        [Description("3 Курс")]
        Course3 = 2,
        [Description("4 Курс")]
        Course4 = 3,
        [Description("5 Курс")]
        Course5 = 4,
    }
}

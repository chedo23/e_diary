﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dto.Registration.AttendanceRegistration;
using Journal.Domain.Registration;

namespace Journal.Bll.Services.Registration
{
    public class AttendanceRegistrationService : BaseRegistrationServiceOperations<AttendanceRegistration, AttendanceRegistrationDto>, IAttendanceRegistrationService
    {
        public AttendanceRegistrationService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

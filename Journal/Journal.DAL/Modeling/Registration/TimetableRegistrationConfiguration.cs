﻿using Journal.Domain.Registration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Registration
{
    public class TimetableRegistrationConfiguration : IEntityTypeConfiguration<TimetableRegistration>
    {
        public void Configure(EntityTypeBuilder<TimetableRegistration> builder)
        {
            builder.HasOne(x => x.Teacher)
                .WithMany(x => x.TimetableRegistrations)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}

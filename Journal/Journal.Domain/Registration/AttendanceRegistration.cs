﻿using Journal.Domain.Common;
using Journal.Domain.Dictionary;
using Journal.Domain.Enums;

namespace Journal.Domain.Registration
{
    /// <summary>
    /// Контроль посещаемости
    /// </summary>
    public class AttendanceRegistration : RegistrationBase, IEntity
    {
        public int Id { get; set; }

        public int TrainingGroupId { get; set; }

        public TrainingGroup TrainingGroup { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        public AttendanceStatus Attendance { get; set; }
    }
}

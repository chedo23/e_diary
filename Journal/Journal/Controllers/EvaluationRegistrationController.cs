﻿using Journal.Core.Services;
using Journal.Domain.Dto.Registration.EvaluationRegistration;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EvaluationRegistrationController : Controller, IOperationController<EvaluationRegistrationDto>
    {
        private readonly IEvaluationRegistrationService _evaluationRegistrationService;

        public EvaluationRegistrationController(IEvaluationRegistrationService evaluationRegistrationService)
        {
            this._evaluationRegistrationService = evaluationRegistrationService;
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] EvaluationRegistrationDto dto)
        {
            await this._evaluationRegistrationService.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EvaluationRegistrationDto dto)
        {
            await this._evaluationRegistrationService.AddAsync(dto);

            return this.Ok();
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._evaluationRegistrationService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._evaluationRegistrationService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._evaluationRegistrationService.RemoveAsync(id);

            return this.Ok();
        }
    }
}
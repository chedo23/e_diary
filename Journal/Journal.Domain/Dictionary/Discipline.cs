﻿using System.Collections.Generic;
using Journal.Domain.Common;
using Journal.Domain.Registration;

namespace Journal.Domain.Dictionary
{
    /// <summary>
    /// Например: Математика, физика, химия и тд.
    /// </summary>
    public class Discipline : DictionaryBase, IEntity
    {
        public int Id { get; set; }

        public List<EvaluationRegistration> EvaluationRegistrations { get; set; }
    }
}

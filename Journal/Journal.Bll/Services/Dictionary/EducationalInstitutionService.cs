﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.EducationalInstitution;

namespace Journal.Bll.Services.Dictionary
{
    public class EducationalInstitutionService : BaseServiceDictionaryOperations<EducationalInstitution, EducationalInstitutionDto>, IEducationalInstitutionService
    {
        public EducationalInstitutionService(ISession session, IMapper mapper) :base(session, mapper)
        {
        }
    }
}

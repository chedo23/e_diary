﻿using Journal.Domain.Common;

namespace Journal.Domain.Dto.Dictionary.Specialty
{
    public class SpecialtyDto : DictionaryBase, IDto
    {
        public int Id { get; set; }
    }
}

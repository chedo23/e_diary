﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.Teacher;

namespace Journal.Bll.Services.Dictionary
{
    public class TeacherService : BaseServiceDictionaryOperations<Teacher, TeacherDto>, ITeacherService
    {
        public TeacherService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

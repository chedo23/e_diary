﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Faculty;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FacultyController : Controller, IOperationController<FacultyDto>
    {
        private readonly IFacultyService _facultyService;

        public FacultyController(IFacultyService facultyService)
        {
            this._facultyService = facultyService;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._facultyService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._facultyService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] FacultyDto dto)
        {
            await this._facultyService.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] FacultyDto dto)
        { 
            await this._facultyService.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._facultyService.RemoveAsync(id);

            return this.Ok();
        }
    }
}
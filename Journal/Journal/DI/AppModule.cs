﻿using Autofac;
using Journal.Bll.DI;

namespace Journal.App.DI
{
    public class AppModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<BllModule>();
        }
    }
}

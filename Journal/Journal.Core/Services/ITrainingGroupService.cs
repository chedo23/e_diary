﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.TrainingGroup;

namespace Journal.Core.Services
{
    public interface ITrainingGroupService : IServiceBase<TrainingGroupDto>
    {
    }
}

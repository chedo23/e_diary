﻿using Journal.Domain.Dto.Common;

namespace Journal.Domain.Dto.Dictionary.Student
{
    public class StudentSearchRequest : SearchRequestBase
    {
        public string NameFilter { get; set; }

        public int StatusFilter { get; set; }

        public int CourseFilter { get; set; }

        public int FacultyFilter { get; set; }

        public int SpecialtyFilter { get; set; }
    }
}

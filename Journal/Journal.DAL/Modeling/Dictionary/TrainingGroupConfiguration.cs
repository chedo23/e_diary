﻿using Journal.DAL.Modeling.Common;
using Journal.Domain.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Dictionary
{
     public class TrainingGroupConfiguration : BaseDictionaryConfiguration<TrainingGroup>, IEntityTypeConfiguration<TrainingGroup>
    {
        public void Configure(EntityTypeBuilder<TrainingGroup> builder)
        {
            base.BaseConfigure(builder);
        }
    }
}
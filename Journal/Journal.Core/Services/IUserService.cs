﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.User;

namespace Journal.Core.Services
{
    public interface IUserService : IServiceBase<UserDto>
    {
    }
}

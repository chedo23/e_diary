﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.Discipline;

namespace Journal.Core.Services
{
    public interface IDisciplineService : IServiceBase<DisciplineDto>
    {
    }
}

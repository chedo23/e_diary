﻿using System;

namespace Journal.Domain.Common
{
    /// <summary>
    /// Базовый класс для всех справочников
    /// </summary>
    public abstract class DictionaryBase
    {
        public string Name { get; set; }

        public string Note { get; set; }

        public DateTime Created { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }
    }
}

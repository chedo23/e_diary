﻿using Journal.Domain.Common;
using Journal.Domain.Enums;

namespace Journal.Domain.Auth
{
    public class User : DictionaryBase, IEntity
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public AuthRole AuthRole { get; set; }

        public string Password { get; set; }
    }
}

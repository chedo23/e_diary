﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.Faculty;

namespace Journal.Bll.Services.Dictionary
{
    public class FacultyService : BaseServiceDictionaryOperations<Faculty, FacultyDto>, IFacultyService
    {
        public FacultyService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

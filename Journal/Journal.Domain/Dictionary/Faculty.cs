﻿using System.Collections.Generic;
using Journal.Domain.Common;

namespace Journal.Domain.Dictionary
{
    /// <summary>
    /// Например: Факультет радиотехники, фукультет животноводства и тд
    /// </summary>
    public class Faculty : DictionaryBase, IEntity
    {
        public int Id { get; set; }

        public List<Specialty> Specialties { get; set; }

        public List<Student> Student { get; set; }

        public List<Discipline> Disciplines { get; set; }
    }
}

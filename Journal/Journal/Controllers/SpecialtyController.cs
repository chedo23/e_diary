﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Specialty;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SpecialtyController : Controller, IOperationController<SpecialtyDto>
    {
        private readonly ISpecialtyService _specialtyService;

        public SpecialtyController(ISpecialtyService specialtyService)
        {
            this._specialtyService = specialtyService;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._specialtyService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._specialtyService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SpecialtyDto dto)
        {
            await this._specialtyService.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] SpecialtyDto dto)
        {
            await this._specialtyService.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._specialtyService.RemoveAsync(id);

            return this.Ok();
        }
    }
}
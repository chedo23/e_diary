﻿using System.Collections.Generic;
using Journal.Domain.Common;

namespace Journal.Domain.Dictionary
{
    /// <summary>
    /// Например: Колледж ККэп, Колледж Науки и градостроения, Колледж Сотовой связи и тд.
    /// </summary>
    public class EducationalInstitution : DictionaryBase, IEntity
    {
        public int Id { get; set; }

        public List<Teacher> Teachers { get; set; }

        public List<Faculty> Faculty { get; set; }
    }
}

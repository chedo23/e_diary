﻿using System.Threading.Tasks;

namespace Journal.XUnit.Test.Common
{
    public interface IOperationControllerTest
    {
        Task List();

        Task Get();

        Task Post();

        Task Put();

        Task Delete();
    }
}

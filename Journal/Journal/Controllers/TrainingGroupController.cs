﻿using Journal.Domain.Dto.Dictionary.TrainingGroup;
using Microsoft.AspNetCore.Mvc;
using Journal.Core.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TrainingGroupController : Controller, IOperationController<TrainingGroupDto>
    {
        private readonly ITrainingGroupService _trainingGroupService;

        public TrainingGroupController(ITrainingGroupService trainingGroupService)
        {
            this._trainingGroupService = trainingGroupService;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._trainingGroupService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._trainingGroupService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TrainingGroupDto dto)
        {
            await this._trainingGroupService.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] TrainingGroupDto dto)
        {
            await this._trainingGroupService.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._trainingGroupService.RemoveAsync(id);

            return this.Ok();
        }
    }
}
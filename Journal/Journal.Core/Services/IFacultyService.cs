﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.Faculty;

namespace Journal.Core.Services
{
    public interface IFacultyService : IServiceBase<FacultyDto>
    {
    }
}

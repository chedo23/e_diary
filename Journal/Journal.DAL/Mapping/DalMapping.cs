﻿using AutoMapper;
using Journal.DAL.Extensions;
using Journal.Domain.Auth;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.Discipline;
using Journal.Domain.Dto.Dictionary.EducationalInstitution;
using Journal.Domain.Dto.Dictionary.User;
using Journal.Domain.Dto.Registration.EvaluationRegistration;
using Journal.Domain.Registration;

namespace Journal.DAL.Mapping
{
    public class DalMapping : Profile
    {
        public DalMapping()
        {
            this.MapDto();
            this.MapSummaryDto();
        }

        private void MapDto()
        {
            this.CreateMap<DisciplineDto, Discipline>();
            this.CreateMap<Discipline, DisciplineDto>();

            this.CreateMap<User, UserDto>()
                .ForMember(x => x.AuthRole,
                    x => x.MapFrom(o => o.AuthRole));

            this.CreateMap<UserDto, User>()
                .ForMember(x => x.AuthRole,
                    x => x.MapFrom(o => o.AuthRole));

            this.CreateMap<EducationalInstitution, EducationalInstitutionDto>();

            this.CreateMap<EvaluationRegistration, EvaluationRegistrationDto>()
                .ForMember(
                    x => x.Evaluation,
                    x => x.MapFrom(o => o.Evaluation.GetDescription()));
        }

        private void MapSummaryDto()
        {

        }
    }
}

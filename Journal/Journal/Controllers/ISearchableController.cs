﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Journal.App.Controllers
{
    public interface ISearchableController<in TRequest>
    {
        Task<IActionResult> Search(TRequest request);
    }
}

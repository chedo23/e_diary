﻿using Journal.Domain.Common;

namespace Journal.Domain.Dto.Dictionary.Discipline
{
    public class DisciplineDto : DictionaryBase, IDto
    {
        public int Id { get; set; }
    }
}

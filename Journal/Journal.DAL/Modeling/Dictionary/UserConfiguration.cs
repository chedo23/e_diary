﻿using Journal.DAL.Modeling.Common;
using Journal.Domain.Auth;
using Journal.Domain.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Dictionary
{
    public class UserConfiguration : BaseDictionaryConfiguration<User>, IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            base.BaseConfigure(builder);
        }
    }
}

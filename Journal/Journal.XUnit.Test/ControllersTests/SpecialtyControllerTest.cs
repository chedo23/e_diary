﻿using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Specialty;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class SpecialtyControllerTest : IOperationControllerTest
    {
        private readonly Mock<ISpecialtyService> _service;

        private readonly Mock<SpecialtyController> _controller;

        public SpecialtyControllerTest()
        {
            _service = new Mock<ISpecialtyService>();
            _controller = new Mock<SpecialtyController>(this._service.Object);
        }

        [Fact]
        [TestInitialize]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [Fact]
        [TestInitialize]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [Fact]
        [TestInitialize]
        public async Task Post()
        {
            var dto = new SpecialtyDto
            {
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Post(dto);
        }

        [Fact]
        [TestInitialize]
        public async Task Put()
        {
            var dto = new SpecialtyDto
            {
                Id = 1,
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Put(dto);
        }

        [Fact]
        [TestInitialize]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }
    }
}

﻿namespace Journal.Domain.Common
{
    /// <summary>
    /// Интерфейс для всех справочников
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
    }
}

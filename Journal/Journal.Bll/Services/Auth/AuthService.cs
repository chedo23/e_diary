﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.DAL.Extensions;
using Journal.Domain.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace Journal.Bll.Services.Auth
{
    public class AuthService : IAuthService
    {
        private readonly ISession _session;

        public AuthService(ISession session)
        {
            this._session = session;
        }

        public async Task<object> GetToken(string login, string password)
        {
            var identity = await this.GetIdentity(login, password);

            if (identity == null)
            {
                return null;
            }

            var token = new JwtSecurityToken(
                issuer: AuthOptions.client,
                audience: AuthOptions.publisher,
                notBefore: DateTime.UtcNow,
                claims: identity.Claims,
                expires: DateTime.UtcNow.Add(TimeSpan.FromDays(AuthOptions.lifeTime)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var jwtEncoded = new JwtSecurityTokenHandler().WriteToken(token);

            return new
            {
                access_token = jwtEncoded,
                username = identity.Name
            };
        }

        public async Task<ClaimsIdentity> GetIdentity(string login, string password)
        {
            var user = await this._session.Query<User>()
                .SingleOrDefaultAsync(x => x.Login == login && x.Password == password && x.IsActive);

            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.AuthRole.GetDescription())
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
    }
}

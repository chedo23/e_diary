﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Journal.DAL.Context;
using Journal.Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace Journal.Bll.Services.Common
{
    /// <summary>
    /// Базовый класс для сервисов таблиц
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TDto"></typeparam>
    public abstract class BaseServiceDictionaryOperations<TEntity, TDto> 
        where TEntity : DictionaryBase, IEntity
        where TDto : class
    {
        private readonly ISession _session;

        private readonly IMapper _mapper;

        protected BaseServiceDictionaryOperations(ISession session, IMapper mapper)
        {
            _session = session;
            _mapper = mapper;
        }

        public virtual async Task AddAsync(TDto dto)
        {
            await this._session.AddEntityAsync(this._mapper.Map<TEntity>(dto));
        }

        public virtual async Task<TDto> GetAsync(int id)
        {
            var result = await this._session.Query<TEntity>()
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.Id == id);

            return this._mapper.Map<TDto>(result);
        }

        public virtual async Task<List<TDto>> ListAsync()
        {
            return await this._session.Query<TEntity>()
                .AsNoTracking()
                .Select(x => this._mapper.Map<TDto>(x))
                .ToListAsync();
        }

        public virtual async Task UpdateAsync(TDto dto)
        {
            await this._session.UpdateEntityAsync(this._mapper.Map<TEntity>(dto));
        }

        public virtual async Task RemoveAsync(int id)
        {
            var result = await this._session.Query<TEntity>()
                .SingleOrDefaultAsync(x => x.Id == id);

            result.IsDeleted = true;

            await this._session.FlushAsync();
        }
    }
}

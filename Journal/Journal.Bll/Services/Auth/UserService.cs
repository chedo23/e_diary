﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Auth;
using Journal.Domain.Dto.Dictionary.User;

namespace Journal.Bll.Services.Auth
{
    public class UserService : BaseServiceDictionaryOperations<User, UserDto>, IUserService
    {
        public UserService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

﻿using Journal.Domain.Common;

namespace Journal.Domain.Dto.Dictionary.Student
{
    public class StudentDto : DictionaryBase, IDto
    {
        public int Id { get; set; }

        public int SpecialtyId { get; set; }

        public Domain.Dictionary.Specialty Specialty { get; set; }

        public int FacultyId { get; set; }

        public Domain.Dictionary.Faculty Faculty { get; set; }
    }
}

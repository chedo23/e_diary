﻿using Journal.Domain.Common;

namespace Journal.Domain.Dto.Dictionary.Teacher
{
    public class TeacherDto : DictionaryBase, IDto
    {
        public int Id { get; set; }

        public int DisciplineId { get; set; }

        public Domain.Dictionary.Discipline Discipline { get; set; }
    }
}

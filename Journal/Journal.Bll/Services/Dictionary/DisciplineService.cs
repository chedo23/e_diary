﻿using System.Threading.Tasks;
using AutoMapper;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dictionary;
using Journal.Domain.Dto.Dictionary.Discipline;
using Journal.Bll.Services.Common;

namespace Journal.Bll.Services.Dictionary
{
    public class DisciplineService : BaseServiceDictionaryOperations<Discipline, DisciplineDto>, IDisciplineService
    {
        public DisciplineService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

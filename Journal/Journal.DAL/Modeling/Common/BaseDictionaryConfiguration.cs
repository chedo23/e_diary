﻿using Journal.Domain.Common;
using Journal.Domain.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Common
{
    /// <summary>
    /// базовый класс для модели справочников
    /// </summary>
    public abstract class BaseDictionaryConfiguration<TEntity> where TEntity : DictionaryBase
    {
        public void BaseConfigure(EntityTypeBuilder<TEntity> builder)
        {
            builder
                .Property(x => x.Name)
                .HasMaxLength(50);

            builder
                .Property(x => x.Note)
                .HasMaxLength(150);

            builder
                .Property(x => x.Created)
                .HasDefaultValueSql("GETDATE()");

            builder
                .Property(x => x.IsDeleted)
                .HasDefaultValue(false);

            builder
                .Property(x => x.IsActive)
                .HasDefaultValue(true);
        }
    }
}

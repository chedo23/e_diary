﻿using System.Threading.Tasks;
using Journal.DAL.Context;
using Microsoft.AspNetCore.Mvc;

namespace Journal.App.Controllers
{
    public interface IOperationController<in TDto>
    {
        [HttpGet]
        Task<IActionResult> List();

        [HttpGet]
        Task<IActionResult> Get(int id);

        [HttpPost]
        Task<IActionResult> Post([FromBody] TDto dto);

        [HttpPut]
        Task<IActionResult> Put([FromBody] TDto dto);

        [HttpDelete]
        Task<IActionResult> Delete(int id);
    }
}

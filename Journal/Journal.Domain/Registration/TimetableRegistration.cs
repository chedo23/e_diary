﻿using Journal.Domain.Common;
using Journal.Domain.Dictionary;

namespace Journal.Domain.Registration
{
    /// <summary>
    /// Формирование расписания
    /// </summary>
    public class TimetableRegistration : RegistrationBase, IEntity
    {
        public int Id { get; set; }

        public int TrainingGroupId { get; set; }

        public TrainingGroup TrainingGroup { get; set; }

        public int TeacherId { get; set; }

        public Teacher Teacher { get; set; }

        public int DisciplineId { get; set; }

        public Discipline Discipline { get; set; }
    }
}

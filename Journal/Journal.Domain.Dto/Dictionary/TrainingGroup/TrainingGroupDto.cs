﻿using System.Collections.Generic;
using Journal.Domain.Common;

namespace Journal.Domain.Dto.Dictionary.TrainingGroup
{
    public class TrainingGroupDto : DictionaryBase, IDto
    {
        public int Id { get; set; }

        public List<Domain.Dictionary.Student> Students { get; set; }
    }
}

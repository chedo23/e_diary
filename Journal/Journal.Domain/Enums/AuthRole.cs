﻿using System.ComponentModel;

namespace Journal.Domain.Enums
{
    public enum AuthRole
    {
        [Description("Администратор")]
        Admin = 0,
        [Description("Гость")]
        Guest = 1
    }
}

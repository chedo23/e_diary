﻿using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Journal.DAL.Extensions;
using Journal.DAL.Modeling.Dictionary;
using Journal.DAL.Modeling.Registration;
using Journal.Domain.Auth;
using Journal.Domain.Common;
using Journal.Domain.Dictionary;
using Journal.Domain.Registration;
using Microsoft.EntityFrameworkCore;

namespace Journal.DAL.Context
{
    public class JournalContext : DbContext
    {
        public JournalContext(DbContextOptions<JournalContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Discipline> Disciplines { get; set; }

        public DbSet<EducationalInstitution> EducationalInstitutions { get; set; }

        public DbSet<Faculty> Faculties { get; set; }

        public DbSet<Specialty> Specialties { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<Teacher> Teachers { get;set; }

        public DbSet<TrainingGroup> TrainingGroups { get; set; }

        public DbSet<AttendanceRegistration> AttendanceRegistrations { get; set; }

        public DbSet<EvaluationRegistration> EvaluationRegistrations { get; set; }

        public DbSet<TimetableRegistration> TimetableRegistrations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Soft delete
            modelBuilder.Entity<Discipline>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<EducationalInstitution>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Faculty>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Specialty>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Student>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Teacher>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<TrainingGroup>().HasQueryFilter(x => !x.IsDeleted);

            //Sequence

            const string schema = "dbo";
            modelBuilder.HasSequence<int>(nameof(Discipline) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(EducationalInstitution) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(Faculty) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(Specialty) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(Student) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(Teacher) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(TrainingGroup) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(AttendanceRegistration) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(EvaluationRegistration)+ "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(TimetableRegistration) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            modelBuilder.HasSequence<int>(nameof(User) + "Code", schema)
                .StartsAt(1)
                .IncrementsBy(1);

            //Apply configs
            //Conf dictionary
            modelBuilder.ApplyConfiguration(new DisciplineConfiguration());
            modelBuilder.ApplyConfiguration(new EducationalInstitutionConfiguration());
            modelBuilder.ApplyConfiguration(new FacultyConfiguration());
            modelBuilder.ApplyConfiguration(new SpecialtyConfiguration());
            modelBuilder.ApplyConfiguration(new StudentConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            //Conf registration
            modelBuilder.ApplyConfiguration(new TrainingGroupConfiguration());
            modelBuilder.ApplyConfiguration(new AttendanceRegistrationConfiguration());
            modelBuilder.ApplyConfiguration(new EvaluationRegistrationConfiguration());
            modelBuilder.ApplyConfiguration(new TimetableRegistrationConfiguration());
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            this.RunTrigger();

            return base.SaveChangesAsync(cancellationToken);
        }
    }
}

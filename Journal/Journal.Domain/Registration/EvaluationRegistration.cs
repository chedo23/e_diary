﻿using Journal.Domain.Common;
using Journal.Domain.Dictionary;
using Journal.Domain.Enums;

namespace Journal.Domain.Registration
{
    /// <summary>
    /// Проставление оценок
    /// </summary>
    /// TODO : добавить валидацию на поле оценки и тд при переносе в BLL modeling
    public class EvaluationRegistration : RegistrationBase, IEntity
    {
        public int Id { get; set; }

        public int StudentId { get; set; }

        public Student Student { get; set; }

        public int DisciplineId { get; set; }

        public Discipline Discipline { get; set; }

        public Evaluation Evaluation { get; set; }
    }
}

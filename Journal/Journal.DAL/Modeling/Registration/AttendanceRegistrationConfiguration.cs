﻿using Journal.Domain.Registration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Registration
{
    public class AttendanceRegistrationConfiguration : IEntityTypeConfiguration<AttendanceRegistration>
    {
        public void Configure(EntityTypeBuilder<AttendanceRegistration> builder)
        {
            builder
                .HasOne(x => x.Student)
                .WithMany(x => x.AttendanceRegistrations)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(x => x.TrainingGroup)
                .WithMany(x => x.AttendanceRegistrations)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
﻿using AutoMapper;
using Journal.Bll.Services.Common;
using Journal.Core.Services;
using Journal.DAL.Context;
using Journal.Domain.Dto.Registration.EvaluationRegistration;
using Journal.Domain.Registration;

namespace Journal.Bll.Services.Registration
{
    public class EvaluationRegistrationService : BaseRegistrationServiceOperations<EvaluationRegistration, EvaluationRegistrationDto>, IEvaluationRegistrationService
    {
        public EvaluationRegistrationService(ISession session, IMapper mapper) : base(session, mapper)
        {
        }
    }
}

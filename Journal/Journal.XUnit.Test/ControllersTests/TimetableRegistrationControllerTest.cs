﻿using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Registration.TimetableRegistration;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class TimetableRegistrationControllerTest : IOperationControllerTest
    {
        private readonly Mock<ITimetableRegistrationService> _service;
        private readonly Mock<TimetableRegistrationController> _controller;
        

        public TimetableRegistrationControllerTest()
        {
            this._service = new Mock<ITimetableRegistrationService>();
            this._controller = new Mock<TimetableRegistrationController>(this._service.Object);
        }

        [TestInitialize]
        [Fact]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [TestInitialize]
        [Fact]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [TestInitialize]
        [Fact]
        public async Task Post()
        {
            var dto = new TimetableRegistrationDto
            {
                Created = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                TrainingGroupId = 666,
                TeacherId = 666,
                DisciplineId = 666,
                IsActive = true
                /* не тестируются:
                 * TrainingGroup
                 * Teacher
                 * Discipline  
                */
            };

            await this._controller.Object.Post(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Put()
        {
            var dto = new TimetableRegistrationDto
            {
                Id = 1,
                Created = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                TrainingGroupId = 666,
                TeacherId = 666,
                DisciplineId = 666,
                IsActive = true
                /* не тестируются:
                 * TrainingGroup
                 * Teacher
                 * Discipline  
                */
            };

            await this._controller.Object.Put(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }
    }

}

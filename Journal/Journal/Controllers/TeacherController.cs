﻿using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Teacher;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TeacherController : Controller, IOperationController<TeacherDto>
    {
        private readonly ITeacherService _teacherService;

        public TeacherController(ITeacherService teacherService)
        {
            this._teacherService = teacherService;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._teacherService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._teacherService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TeacherDto dto)
        {
            await this._teacherService.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] TeacherDto dto)
        {
            await this._teacherService.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._teacherService.RemoveAsync(id);

            return this.Ok();
        }
    }
}
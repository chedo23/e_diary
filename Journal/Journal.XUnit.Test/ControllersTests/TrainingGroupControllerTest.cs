﻿using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.TrainingGroup;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class TrainingGroupControllerTest : IOperationControllerTest
    {
        private readonly Mock<ITrainingGroupService> _service;

        private readonly Mock<TrainingGroupController> _controller;

        public TrainingGroupControllerTest()
        {
            _service = new Mock<ITrainingGroupService>();
            _controller = new Mock<TrainingGroupController>(this._service.Object);
        }

        [Fact]
        [TestInitialize]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }

        [Fact]
        [TestInitialize]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [Fact][TestInitialize]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [Fact]
        [TestInitialize]
        public async Task Post()
        {
            var dto = new TrainingGroupDto
            {
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Post(dto);
        }

        [Fact]
        [TestInitialize]
        public async Task Put()
        {
            var dto = new TrainingGroupDto
            {
                Id = 1,
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Put(dto);
        }
    }
}
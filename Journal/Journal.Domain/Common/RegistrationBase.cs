﻿using System;

namespace Journal.Domain.Common
{
    /// <summary>
    /// Базовый класс для всех регистрационных DTO
    /// </summary>
    public abstract class RegistrationBase
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime Created { get; set; }

        public bool IsActive { get; set; }
    }
}

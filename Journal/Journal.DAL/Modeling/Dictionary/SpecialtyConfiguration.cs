﻿using Journal.DAL.Modeling.Common;
using Journal.Domain.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Dictionary
{
    public class SpecialtyConfiguration : BaseDictionaryConfiguration<Specialty>, IEntityTypeConfiguration<Specialty>
    {
        public void Configure(EntityTypeBuilder<Specialty> builder)
        {
            base.BaseConfigure(builder);
        }
    }
}
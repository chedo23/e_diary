﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Journal.DAL.Context
{
    public class Session : ISession
    {
        private readonly JournalContext _context;

        public Session(JournalContext context)
        {
            this._context = context;
        }

        public async Task AddEntityAsync<TEntity>(TEntity entity) where TEntity : class 
        {
            await this._context.Set<TEntity>().AddAsync(entity);
            await this.FlushAsync();
        }

        public async Task RemoveEntity<TEntity>(TEntity entity) where TEntity : class
        {
            this._context.Set<TEntity>().Remove(entity);
            await this.FlushAsync();
        }

        public async Task UpdateEntityAsync<TEntity>(TEntity entity) where TEntity : class
        {
            this._context.Set<TEntity>().Update(entity);
            await this.FlushAsync();
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class
        {
            return this._context.Set<TEntity>().AsQueryable();
        }

        public async Task<List<TEntity>> SqlQueryAsync<TEntity>(string sqlCommand, params object[] parameters) where TEntity : class
        { 
            return await this._context.Set<TEntity>().FromSqlRaw(sqlCommand, parameters).ToListAsync();
        }

        public async Task<List<TEntity>> SqlQueryAsync<TEntity>(string sqlCommand) where TEntity : class
        {
            return await this._context.Set<TEntity>().FromSqlRaw(sqlCommand).ToListAsync();
        }

        public async Task FlushAsync()
        {
            await this._context.SaveChangesAsync();
        }
    }
}

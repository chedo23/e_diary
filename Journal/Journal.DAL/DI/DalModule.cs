﻿using Autofac;
using Journal.DAL.Context;

namespace Journal.DAL.DI
{
    public class DalModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Session>().As<ISession>().SingleInstance();
            builder.RegisterAssemblyTypes(this.GetType().Assembly).AsImplementedInterfaces();
        }
    }
}

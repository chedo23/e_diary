﻿using System.Collections.Generic;
using Journal.Domain.Common;

namespace Journal.Domain.Dto.Dictionary.Faculty
{
    public class FacultyDto : DictionaryBase, IDto
    {
        public int Id { get; set; }

        public List<Domain.Dictionary.Specialty> Specialties { get; set; }

        public List<Domain.Dictionary.Student> Student { get; set; }

        public List<Domain.Dictionary.Discipline> Disciplines { get; set; }
    }
}

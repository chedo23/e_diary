﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.EducationalInstitution;

namespace Journal.Core.Services
{
    public interface IEducationalInstitutionService : IServiceBase<EducationalInstitutionDto>
    {
    }
}

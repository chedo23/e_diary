﻿using Journal.Core.Services.Common;
using Journal.Domain.Dto.Registration.AttendanceRegistration;

namespace Journal.Core.Services
{
    public interface IAttendanceRegistrationService : IServiceBase<AttendanceRegistrationDto>
    {
    }
}

﻿using Journal.Domain.Common;
using Journal.Domain.Enums;

namespace Journal.Domain.Dto.Dictionary.User
{
    public class UserDto : DictionaryBase, IDto
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public int AuthRole { get; set; }

        public string Password { get; set; }
    }
}

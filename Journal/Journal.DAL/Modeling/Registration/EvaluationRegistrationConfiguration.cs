﻿using Journal.Domain.Registration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Journal.DAL.Modeling.Registration
{
    public class EvaluationRegistrationConfiguration : IEntityTypeConfiguration<EvaluationRegistration>
    {
        public void Configure(EntityTypeBuilder<EvaluationRegistration> builder)
        {
            builder
                .HasOne(x => x.Student)
                .WithMany(x => x.EvaluationRegistrations)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(x => x.Discipline)
                .WithMany(x => x.EvaluationRegistrations)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}

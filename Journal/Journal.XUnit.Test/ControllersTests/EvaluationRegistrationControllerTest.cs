﻿using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Registration.EvaluationRegistration;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class EvaluationRegistrationControllerTest : IOperationControllerTest
    {
        private readonly Mock<IEvaluationRegistrationService> _service;
        private readonly Mock<EvaluationRegistrationController> _controller;

        public EvaluationRegistrationControllerTest()
        {
            this._service = new Mock<IEvaluationRegistrationService>();
            this._controller = new Mock<EvaluationRegistrationController>(this._service.Object);
        }

        [TestInitialize]
        [Fact]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [TestInitialize]
        [Fact]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [TestInitialize]
        [Fact]
        public async Task Post()
        {
            var dto = new EvaluationRegistrationDto
            {
                Created = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                DisciplineId = 666,
                StudentId = 666,
                IsActive = true
                /* не тестируются:
                 * Student
                 * Discipline
                 * Evaluation
                */
            };

            await this._controller.Object.Post(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Put()
        {
            var dto = new EvaluationRegistrationDto
            {
                Id = 1,
                Created = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                DisciplineId = 666,
                StudentId = 666,
                IsActive = true
                /* не тестируются:
                 * Student
                 * Discipline
                 * Evaluation
                */
            };

            await this._controller.Object.Put(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }
    }
}

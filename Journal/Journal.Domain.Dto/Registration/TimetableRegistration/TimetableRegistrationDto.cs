﻿using Journal.Domain.Common;
using Journal.Domain.Dictionary;

namespace Journal.Domain.Dto.Registration.TimetableRegistration
{
    public class TimetableRegistrationDto : RegistrationBase, IDto
    {
        public int Id { get; set; }

        public int TrainingGroupId { get; set; }

        public TrainingGroup TrainingGroup { get; set; }

        public int TeacherId { get; set; }

        public Teacher Teacher { get; set; }

        public int DisciplineId { get; set; }

        public Discipline Discipline { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Journal.Domain.Common;

namespace Journal.DAL.Context
{
    public interface ISession
    {
        Task AddEntityAsync<TEntity>(TEntity entity) 
            where TEntity : class; 

        Task RemoveEntity<TEntity>(TEntity entity) 
            where TEntity : class;

        Task UpdateEntityAsync<TEntity>(TEntity entity)
        where TEntity : class;

        IQueryable<TEntity> Query<TEntity>()
        where TEntity : class;

        Task<List<TEntity>> SqlQueryAsync<TEntity>(string sqlCommand, params object[] parameters) 
            where TEntity : class;

        Task<List<TEntity>> SqlQueryAsync<TEntity>(string sqlCommand)
            where TEntity : class;

        Task FlushAsync();
    }
}

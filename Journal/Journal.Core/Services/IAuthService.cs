﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace Journal.Core.Services
{
    public interface IAuthService
    {
        Task<object> GetToken(string login, string password);

        Task<ClaimsIdentity> GetIdentity(string login, string password);
    }
}

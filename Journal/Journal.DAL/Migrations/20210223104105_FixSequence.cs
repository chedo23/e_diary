﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Journal.DAL.Migrations
{
    public partial class FixSequence : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropSequence(
                name: "AttendanceRegistrationAttendanceRegistrationCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "DisciplineDisciplineCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "EducationalInstitutionEducationalInstitutionsCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "EvaluationRegistrationEvaluationRegistrationCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "FacultyFacultyCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "SpecialtySpecialtyCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "StudentStudentCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "TeacherTeacherCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "TimetableRegistrationTimetableRegistrationCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "TrainingGroupTrainingGroupCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "AttendanceRegistrationCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "DisciplineCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "EducationalInstitutionCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "EvaluationRegistrationCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "FacultyCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "SpecialtyCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "StudentCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "TeacherCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "TimetableRegistrationCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "TrainingGroupCode",
                schema: "dbo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropSequence(
                name: "AttendanceRegistrationCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "DisciplineCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "EducationalInstitutionCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "EvaluationRegistrationCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "FacultyCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "SpecialtyCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "StudentCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "TeacherCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "TimetableRegistrationCode",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "TrainingGroupCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "AttendanceRegistrationAttendanceRegistrationCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "DisciplineDisciplineCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "EducationalInstitutionEducationalInstitutionsCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "EvaluationRegistrationEvaluationRegistrationCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "FacultyFacultyCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "SpecialtySpecialtyCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "StudentStudentCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "TeacherTeacherCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "TimetableRegistrationTimetableRegistrationCode",
                schema: "dbo");

            migrationBuilder.CreateSequence<int>(
                name: "TrainingGroupTrainingGroupCode",
                schema: "dbo");
        }
    }
}

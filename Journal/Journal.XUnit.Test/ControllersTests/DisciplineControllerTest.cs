using System;
using System.Threading.Tasks;
using Journal.App.Controllers;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.Discipline;
using Journal.XUnit.Test.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace Journal.XUnit.Test.ControllersTests
{
    [TestClass]
    public class DisciplineControllerTest : IOperationControllerTest
    {
        private readonly Mock<IDisciplineService> _service;

        private readonly Mock<DisciplineController> _controller;

        public DisciplineControllerTest()
        {
            _service = new Mock<IDisciplineService>();
            _controller = new Mock<DisciplineController>(this._service.Object);
        }

        [TestInitialize]
        [Fact]
        public async Task List()
        {
            await this._controller.Object.List();
        }

        [TestInitialize]
        [Fact]
        public async Task Get()
        {
            await this._controller.Object.Get(1);
        }

        [TestInitialize]
        [Fact]
        public async Task Post()
        {
            var dto = new DisciplineDto
            {
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Post(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Put()
        {
            var dto = new DisciplineDto
            {
                Id = 1,
                Name = "Test",
                Note = "Test",
                Created = DateTime.Now,
                IsActive = true,
                IsDeleted = false
            };

            await this._controller.Object.Put(dto);
        }

        [TestInitialize]
        [Fact]
        public async Task Delete()
        {
            await this._controller.Object.Delete(1);
        }
    }
}

﻿using System.Threading.Tasks;
using Journal.Core.Services;
using Journal.Domain.Dto.Dictionary.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : Controller, IOperationController<UserDto>
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._service.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._service.GetAsync(id);

            return this.Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDto dto)
        {
            await this._service.AddAsync(dto);

            return this.Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] UserDto dto)
        {
            await this._service.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._service.RemoveAsync(id);

            return this.Ok();
        }
    }
}

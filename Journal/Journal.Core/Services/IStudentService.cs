﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Journal.Core.Services.Common;
using Journal.Domain.Dto.Dictionary.Student;

namespace Journal.Core.Services
{
    public interface IStudentService : IServiceBase<StudentDto>
    {
        Task<List<StudentDto>> Search(StudentSearchRequest request);
    }
}

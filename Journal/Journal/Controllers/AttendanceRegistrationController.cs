﻿using Journal.Core.Services;
using Journal.Domain.Dto.Registration.AttendanceRegistration;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Journal.App.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AttendanceRegistrationController : Controller, IOperationController<AttendanceRegistrationDto>
    {
        private readonly IAttendanceRegistrationService _attendanceRegistrationService;

        public AttendanceRegistrationController(IAttendanceRegistrationService attendanceRegistrationService)
        {
            this._attendanceRegistrationService = attendanceRegistrationService;
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] AttendanceRegistrationDto dto)
        {
            await this._attendanceRegistrationService.UpdateAsync(dto);

            return this.Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AttendanceRegistrationDto dto)
        {
            await this._attendanceRegistrationService.AddAsync(dto);

            return this.Ok();
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var result = await this._attendanceRegistrationService.ListAsync();

            return this.Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await this._attendanceRegistrationService.GetAsync(id);

            return this.Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await this._attendanceRegistrationService.RemoveAsync(id);

            return this.Ok();
        }
    }
}
﻿using System.Collections.Generic;
using Journal.Domain.Common;
using Journal.Domain.Registration;

namespace Journal.Domain.Dictionary
{
    /// <summary>
    /// Например: Сарокина С.С. Химия
    /// </summary>
    public class Teacher : DictionaryBase, IEntity
    {
        public int Id { get; set; }

        public int DisciplineId { get; set; }

        public Discipline Discipline { get; set; }

        public List<TimetableRegistration> TimetableRegistrations { get; set; }
    }
}
